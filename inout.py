import csv
import ast
import logging

results_keys = ['id', 'init', 'settings', 'thresholds', 'start',
                'detect', 'end', 'timeout', 'costs', 'values',
                'impacts', 'total_cost', 'budget_spent', 'efficiency']


def write_resultsfile(filename, scenarios):
    global results_keys
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=results_keys)
        writer.writeheader()
        for s in scenarios:
            if s.finished:
                writer.writerow(s.stats())


def read_resultsfile(name):
    global results_keys
    string_values = ['init', 'id']
    with open(name, 'r') as infile:
        records = csv.DictReader(infile)
        for r in records:
            res = {}
            for k, v in r.items():
                if k not in results_keys:
                    logging.warn('unknown key')
                    continue
                if k in string_values:
                    res[k] = str(v)
                else:
                    res[k] = ast.literal_eval(v)
            yield res

def write_results(filename):
    with open(filename, 'w', newline='') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=results_keys)
        writer.writeheader()
        while True:
            s = (yield)
            if s.finished:
                writer.writerow(s.stats())



if __name__ == '__main__':
    meuk = read_resultsfile('testdata')
    for m in meuk:
        print(m)
