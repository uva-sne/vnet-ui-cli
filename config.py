import asyncio
import json 

from ssl import SSLContext
from client import SarnetUIClient



async def skeleton_config(uri='wss://skynet.lab.uvalight.net:8101'):
    """ Outputs skeleton config file """
    ctx = SSLContext()
    async with SarnetUIClient(uri, ssl=ctx) as c:
        recv_task = asyncio.ensure_future(c.recv_handler())
        await asyncio.sleep(1)
        nodes = list(c.topology['nodes'].keys())
        config = {'nodecfg': {}}
        for node in nodes:
            config['nodecfg'][node] = {'behaviour': {'delay': 0,
                                                     'success': 1},
                                       'costs': {'default':
                                                 {'fixed': 0,
                                                  'periodic': 0}}}

        print(json.dumps(config, indent=2, sort_keys=True))
        await c.stop()
        recv_task.cancel()
