# CLI based UI client for vnet

Starts and stops attacks on the sarnet network and captures various relevant
metrics. Goal is to automate running (performance) experiments.

- Partially based on the UI javascript code
- Uses the vnet UI interface
- Does not present an actual UI
