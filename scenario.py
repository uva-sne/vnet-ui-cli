import json
import datetime
import logging
from integrate import integrate, calc_efficiency2


class Scenario():

    def __init__(self, attack, attackers, victims,
                 level='local',
                 budget=1000,
                 defense=None,
                 timeout=60,
                 weights=None,
                 ):
        self._timeout = timeout
        self.attack = attack
        self.attackers = attackers
        self.rate = .3
        self.defense = defense
        self.level = level
        self.behaviour = None
        self.time = None
        self.thresholds = None
        self.finished = False
        self.timed_out = False
        self._detect = 0
        self._end = 0
        self._start = 0
        self._t = 0
        self.total_cost = 0
        self.cost_timeline = []
        self.impacts = {}
        self.costs = {}
        self.implemented = {}
        self.negates = {}

        self.weights = {'pw': {'logfails': 0.8, 'logins': 0.2},
                        'ddos': {'sales': 1.0},
                        'reflect': {'sales': 1.0}}
        self.beta = 0.8
        self.alpha = .5
        self.budget = budget
        self.learning = set()
        self.runonce = False

        self.victims = victims
        self.efficiency = {}
        self.values = {}
        for victim in victims:
            self.values[victim] = {}
            self.impacts[victim] = {}
            self.efficiency[victim] = -1

        if weights:
            self.weights.update(weights)
            
    def t(self):
        if self._start:
            self._t = (datetime.datetime.now() - self._start).seconds
        else:
            self._t = 0
        return self._t

    def set_callbacks(self, client):
        client.set_callback('c/service64', self.monitor_victims)
        client.set_callback('cmd/host-netctl', self.track_countermeasures)

    def set_thresholds(self, thresholds):
        self.thresholds = thresholds

    def track_countermeasures(self, name, key, value):
        self.t()
        logging.info('implemented: %s, %s, %s', name, key, value)
        cm = None
        remove = False
        if key == 'cmd/host-netctl/iface-filter':
            cm = 'filter'
        elif key == 'cmd/host-netctl/sdn-redirect':
            cm = 'nfv'
        elif key == 'cmd/host-netctl/nfv':
            cm = 'nfv'

        if value == '[]':  # = removed?
            remove = True

        if cm:
            start = self._t
            impl = self.implemented.setdefault(name, {}).setdefault(cm, [])
            if not remove:
                impl.append((start,))
            else:
                if len(impl) < 1:
                    return
                # lookup the tuple with 1 element and add the end time
                se = list(impl.pop())
                if len(se) < 2:
                    se.append(self._t)
                    impl.append(tuple(se))
                else:
                    logging.warn('WTF race in track_countermeasures %s', impl)
                # can probably be removed but let's keep it for now
                # for i, v in enumerate(impl):
                #     if len(v) < 2:
                #         se = list(impl.pop(i))
                #         se.append(self._t)
                #         impl.insert(i, tuple(se))
            logging.debug('attacks implemented at %s', self.implemented)

    def monitor_victims(self, name, key, value):
        self.t()
        if self._start and not self._end:
            if name in self.victims:
                key = key.split('/')[2]

                if key == 'logfails':
                    lf = json.loads(value)
                    self.values[name].setdefault(key, {})[self._t] = sum(lf.values())
                else:
                    self.values[name].setdefault(key, {})[self._t] = float(value)

    def __str__(self):
        a = [a.split('.')[0] for a in self.attackers]
        v = [v.split('.')[0] for v in self.victims]
        s = '' + self.attack + '(' + str(v) + ',' + str(a) + ',' + \
            str(self.level) + ',' + str(self.rate) + ')'
        return s

    def stats(self):
        stats = {'id': str(self),
                 'settings': str({'weights': self.weights,
                                  'negates': self.negates,
                                  'timeout': self._timeout,
                                  'budget': self.budget,
                                  'alpha': self.alpha,
                                  'beta': self.beta}),
                 'thresholds': str(self.thresholds),
                 'init': self._start,
                 'start': 0,
                 'detect': self._detect,
                 'end': self._end,
                 'timeout': self.timed_out,
                 'costs': self.cost_timeline,
                 'budget_spent': (self.total_cost / self.budget),
                 'values': str(self.values),
                 'impacts': self.impacts,
                 'efficiency': str(self.efficiency),
                 'total_cost': self.total_cost}
        return stats

    def start(self):
        self._start = datetime.datetime.now()
        self._t = self._start
        return self

    def stop(self):
        end = self._t
        self._end = end
        self.finished = True
        self.generate_costs()
        if self._detect:
            logging.debug('self.detect:%s', str(self._detect))
            self.calc_impact_efficiency()
        logging.info('attack duration: %ss', str(end))

    def attacked(self, metadata):
        monitored = {k: v for k, v in metadata.items()
                     for vic in self.victims if vic in k}
        logging.debug('health check on: %s', [m for m in monitored.keys()])
        for k, v in monitored.items():
            if 'status' in v:
                obs = {ob: val for ob, val in v['status'].items()
                       if 'sarnet/o' in ob}
                att = {ob: val for ob, val in v['status'].items()
                       if 'sarnet/a' in ob}
                if len(obs) > 0:
                    # observables are violated, but not yet attack
                    logging.debug('obs: %s', obs)
                if len(att) > 0:
                    logging.debug('att: %s', att)
                    # recovery changes to 'healthy' (how does this work in run
                    # 2)
                    health = [True for v in att.values() if v == 'healthy']
                    if len(health) > 0 and all(health):
                        return False
                    return True
        return False

    def detect(self):
        self._detect = self._t
        logging.info('attack detected at: %s', str(self._detect))

    def timeout(self):
        self.t()
        if self._detect:
            if self._t - self._detect >= self._timeout:
                # attack is detected
                logging.info('timeout at: %s, %s', self._t,
                             self._t - self._detect)
                self.timed_out = True
        else:
            if self._t >= self._timeout:
                # attack is NOT detected
                logging.info('timeout (no attack detected)  at: %s, %s',
                             self._t, self._t - self._detect)
                self.timed_out = True

        return self.timed_out

    def calc_impact_efficiency(self, alpha=0.5):
        """ Calculates impact and efficiency from detect to
        resolved/timeout)"""
        def _calc_impact_efficiency(victim, alpha):
            impacts = {}
            thresh = self.thresholds[victim]
            negates = {}
            for metric in self.values[victim].keys():
                if metric == 'sales':
                    negates[metric] = True
                else:
                    negates[metric] = False

                times_rel = [int(v) for v in list(self.values[victim][metric].keys())]
                metrics = [float(v) for v in list(self.values[victim][metric].values())]

                # limit range to actual attack
                times_rel = self.limit_range(times_rel)
                metrics = self.limit_range(metrics)

                print('integrating', metric)
                impacts[metric] = float(integrate(float(thresh[metric]),
                                        times_rel,
                                        metrics,
                                        times_rel[0],
                                        times_rel[-1],
                                        normalize=False,
                                        negate=negates[metric],
                                        discard_negative=True))
                print('integratated', metric, negates[metric], impacts[metric])

            self.negates = negates
            self.impacts[victim] = impacts
            recovered = not self.timed_out

            # Division point between recovered/non recovered
            # High is more accurate effectiveness for recovered
            # Everything below 1-b is not recovered
            # recovered > 0.2 > not recovered
            b = self.beta
            # impact vs cost ratio 1 = only impact 0 = only cost
            a = self.alpha
            efficiency = calc_efficiency2(self.attack,
                                          impacts,
                                          self.weights[self.attack],
                                          thresh,
                                          negates,
                                          self._timeout,
                                          self.total_cost,
                                          self.budget,
                                          recovered=recovered,
                                          alpha=a * (1 - b),  # 0 < a < 1 - beta
                                          beta=b  # division point
                                          )
            n_costs = min([self.total_cost / self.budget, 1])
            self.efficiency[victim] = efficiency
            if self.efficiency[victim] > 1:
                logging.warn("efficiency > 1: %s, eff=%s cost=%s", self.efficiency,
                             efficiency, n_costs)

        for victim in self.victims:
            _calc_impact_efficiency(victim, alpha)

    def limit_range(self, metric, t_start=None, t_end=None):
        """Limit the range to t_start, and t_end"""
        if not t_start:
            t_start = self._detect
        if not t_end:
            t_end = self._end
        return metric[t_start: t_end]

    def gen_timeline(self, cm, cost, active):
        n_periods = len(active)
        start = 0
        stop = 1
        cur_period = 0
        on = False
        timeline = [0] * self._timeout
        if n_periods < 1:
            return timeline
        for t, v in enumerate(timeline):
            if t == int(active[cur_period][start]):
                on = True
                timeline[t] += cost['fixed']
            if on:
                if len(active[cur_period]) == 1:
                    end = self._end
                else:
                    end = active[cur_period][stop]
                if t == end:
                    on = False
                    if cur_period < n_periods - 1:
                        cur_period += 1
                timeline[t] += cost['periodic']
        return timeline

    def generate_costs(self):
        """ Creates a cost timeline from detect to resolved/timeout)"""
        implemented = self.implemented

        cm_costs = []
        for node, cms in implemented.items():
            for cm, active in cms.items():
                c = self.costs[node]['default'].copy()
                tl = self.gen_timeline(cm, c, active)
                cm_costs.append(tl)
        self.cost_timeline = [sum(e) for e in zip(*cm_costs)]
        self.total_cost = sum(self.limit_range(self.cost_timeline))
