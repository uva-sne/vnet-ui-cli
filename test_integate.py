import unittest
import pytest
import integrate as subj
from inout import read_resultsfile
from hypothesis import given, settings
from hypothesis.strategies import floats, lists


class TestMetricEfficiency(unittest.TestCase):
    """def calc_metric_efficiency(impact, thresh, timeout):"""
    def test_metric_maxefficiency_equals_impact(self):
        impact = 500
        thresh = 50
        timeout = 10

        # max_efficiency = (thresh * timeout)
        # efficiency = (max_efficiency - impact) / max_efficiency
        # 50 * 10 = 500; 500 - 500 / 500  = 0
        res = subj.calc_metric_efficiency(impact, thresh, True, timeout)
        self.assertEqual(res, 0)

    def test_metric_max_efficiency_impact_zero(self):
        impact = 0
        thresh = 50
        timeout = 20

        # max_efficiency = (thresh * timeout)
        # efficiency = (max_efficiency - impact) / max_efficiency
        # 50 * 20 = 100; (1000 - 500) / 1000  = 0

        res = subj.calc_metric_efficiency(impact, thresh, True, timeout)
        self.assertEqual(res, 1)

    def test_metric_max_efficiency_biggerthan_impact(self):
        impact = 500
        thresh = 50
        timeout = 20

        # max_efficiency = (thresh * timeout)
        # efficiency = (max_efficiency - impact) / max_efficiency
        # 50 * 20 = 100; (1000 - 500) / 1000  = 0

        res = subj.calc_metric_efficiency(impact, thresh, True, timeout)
        self.assertEqual(res, .5)

    def test_metric_max_efficiency_smallerthan_impact(self):
        impact = 500
        thresh = 50
        timeout = 1

        # max_efficiency = (thresh * timeout)
        # efficiency = (max_efficiency - impact) / max_efficiency
        # 50 * 1 = 50; (50 - 500) / 500  = -150
        # negative should raise error
        with self.assertRaises(ValueError,
                               msg='impact is more than max_efficiency'):
            subj.calc_metric_efficiency(impact, thresh, True, timeout)


class TestCounterMeasureEfficiency(unittest.TestCase):
    """Testing:  calc_cm_efficiency(metric_efficiencies, weights):"""
    def test_cm_efficiency_zero(self):
        weights = {'a': 0.0, 'b': 1, 'c': 0}
        metric_efficiencies = {'a': 50.5, 'b': 0, 'c': 30.0}
        # 0.0 * 50.5 + 1 * 0 + 0 * 30 == 0
        res = subj.calc_cm_efficiency(metric_efficiencies, weights)
        self.assertEqual(res, 0)

    def test_cm_efficiency_some_value(self):
        weights = {'a': 0.1, 'b': 0.65, 'c': 0.25}
        metric_efficiencies = {'a': 50, 'b': 20.6, 'c': 30.0}
        #  0.1 * 50 + 0.65 * 20.6 + 0.25 * 30 = 25.89
        res = subj.calc_cm_efficiency(metric_efficiencies, weights)
        self.assertEqual(res, 25.89)

    def test_cm_efficiency_negative_weight(self):
        weights = {'a': 0.1, 'b': 0.5, 'c': -0.7}
        metric_efficiencies = {'a': 50, 'b': 20.6, 'c': 30.0}
        #  0.1 + 0.6 + -0.7 == -1
        with self.assertRaises(ValueError, msg='sum weights < 0'):
            subj.calc_cm_efficiency(metric_efficiencies, weights)

    def test_cm_efficiency_weights_sum_over_one(self):
        weights = {'a': 0.1, 'b': 0.9, 'c': 0.0001}
        metric_efficiencies = {'a': 50, 'b': 20, 'c': 30.0}
        #  0.1 + 0.9 + 0.0001 == 1.0001
        with self.assertRaises(ValueError, msg='sum weights > 1'):
            subj.calc_cm_efficiency(metric_efficiencies, weights)

    def test_cm_efficiency_weights_sum_under_one(self):
        weights = {'a': 0.1999, 'b': 0.6, 'c': 0.2}
        metric_efficiencies = {'a': 50, 'b': 20, 'c': 30.0}
        #  0.1999 + 0.6 + 0.2 == 0.9999
        with self.assertRaises(ValueError, msg='sum weights < 1'):
            subj.calc_cm_efficiency(metric_efficiencies, weights)


class TestEfficiency(unittest.TestCase):
    """
    Testing: calc_def_efficiency(attack, cm_efficiency, costs, budget,
                     recovered=True, alpha=0.5, beta=0.5):
    """
    def test_metric_max_efficiency_out_of_bounds(self):
        cm_efficiency = 50
        costs = 10
        budget = 1000
        recovered = True
        alpha = 0.5
        beta = 0.5
        with self.assertRaises(ValueError,
                               msg='cm_efficiency out of bounds [0,1]'):
            subj.calc_def_efficiency(cm_efficiency,
                                     costs,
                                     budget,
                                     recovered,
                                     alpha,
                                     beta
                                     )

    def test_efficiency_recovered(self):
        cm_efficiency = 0.3
        costs = 250
        budget = 1000
        recovered = True
        alpha = 0.5
        beta = 0.5

        # 250/1000 = 0.25
        res = subj.calc_def_efficiency(cm_efficiency,
                                       costs,
                                       budget,
                                       recovered,
                                       alpha,
                                       beta
                                       )
        self.assertEqual(res, 0.65)

    def test_efficiency_notrecovered(self):
        cm_efficiency = 0.3
        costs = 250
        budget = 1000
        recovered = False
        alpha = 0.5
        beta = 0.5

        # max_efficiency = (thresh * timeout)
        # efficiency = (max_efficiency - impact) / max_efficiency
        # 50 * 10 = 500; 500 - 500 / 500  = 0
        res = subj.calc_def_efficiency(cm_efficiency,
                                       costs,
                                       budget,
                                       recovered,
                                       alpha,
                                       beta
                                       )
        self.assertEqual(res, 0.15)

    def test_metric_max_efficiency_smallerthan_impact(self):
        impact = 500
        thresh = 50
        timeout = 1

        # max_efficiency = (thresh * timeout)
        # efficiency = (max_efficiency - impact) / max_efficiency
        # 50 * 1 = 50; (50 - 500) / 500  = -150
        # negative should raise error
        with self.assertRaises(ValueError,
                               msg='impact is more than max_efficiency'):
            subj.calc_metric_efficiency(impact, thresh, True, timeout)


class TestIntegrate(unittest.TestCase):
    """ integrate(threshold, xs, ys, start, end,
        negate=False, normalize=False, debug=False):
    """
    @given(lists(floats(allow_infinity=True), min_size=10))
    @settings(max_examples=100)
    def test_integral_integrate1_and_integrate2_equal(self, ys):
        xs = list(range(len(ys)))
        thresh = 15
        start = 0
        end = len(ys)
        normalize = False
        for negate in [True, False]:
            res3 = subj.integrate(thresh, xs, ys, start, end, negate,
                                  normalize, discard_negative=True,
                                  debug=False)
            res2 = subj.integrate2(thresh, xs, ys, start, end, negate,
                                   normalize, discard_negative=True,
                                   debug=False)
            res = subj.integrate1(thresh, xs, ys, start, end, not negate,
                                  normalize, False)
            self.assertEqual(res, res2, res3)

    @given(lists(floats(allow_infinity=True), min_size=0))
    @settings(max_examples=100)
    @pytest.mark.filterwarnings('ignore:RuntimeWarning')
    def test_integral_abs_negative_equals_positive(self, ys):
        xs = list(range(len(ys)))
        thresh = 15
        start = 0
        end = len(ys)
        normalize = False
        negate = True
        res = subj.integrate2(thresh, xs, ys, start, end, negate, normalize,
                              discard_negative=False, debug=False)
        negate = False
        res2 = subj.integrate2(thresh, xs, ys, start, end, negate, normalize,
                               discard_negative=False, debug=False)
        self.assertEqual(abs(res), abs(res2))

    def test_integral_zero(self):
        xs = list(range(21))
        ys = [20, 10] * int(len(xs) / 2)
        thresh = 15
        start = 0
        end = len(ys)
        normalize = False

        for negate in [True, False]:
            res = subj.integrate2(thresh, xs, ys, start, end, negate,
                                  normalize, debug=False,
                                  discard_negative=False)
            self.assertEqual(res, 0)

    def test_integral_start_more_equals_end(self):
        xs = list(range(21))
        ys = [20, 10] * int(len(xs) / 2)
        thresh = 15
        normalize = False
        for negate in [True, False]:
            start = 2
            end = 2
            res = subj.integrate2(thresh, xs, ys, start, end, negate,
                                  normalize, False)
            self.assertEqual(res, 0)
            start = 2
            end = 2
            res = subj.integrate2(thresh, xs, ys, start, end, negate,
                                  normalize, False)
            self.assertEqual(res, 0)

    def test_integral_xs_ys_size_unequal(self):
        xs = list(range(10))  # 11 elements
        ys = [20] * 21  # 21 elements
        start = 0
        end = len(xs)
        thresh = 15
        normalize = False
        for negate in [True, False]:
            end = len(xs)
            res = subj.integrate2(thresh, xs, ys, start, end, negate,
                                  normalize, discard_negative=False,
                                  debug=False)
            self.assertEqual(abs(res), 45)
            end = len(ys)
            res = subj.integrate2(thresh, xs, ys, start, end, negate,
                                  normalize, discard_negative=False,
                                  debug=False)
            self.assertEqual(abs(res), 45)


class TestFullEfficiency(unittest.TestCase):
    def test_real_data(self):
        name = 'testdata'
        records = read_resultsfile(name)
        for r in records:
            attack = r['id'].split('(')[0]
            settings = r['settings']
            a = float(settings['alpha'])
            b = float(settings['beta'])
            for node, impacts in r['impacts'].items():
                if not impacts:
                    #  a run may not have detected anything
                    #  and thus have no impacts
                    continue
                efficiency = subj.calc_efficiency2(
                    attack,
                    impacts,
                    settings['weights'][attack],
                    r['thresholds'][node],
                    settings['negates'],
                    int(settings['timeout']),
                    int(r['total_cost']),
                    int(settings['budget']),
                    recovered=not r['timeout'],
                    alpha=a * (1 - b),  # 0 < a < 1 - beta
                    beta=b  # division point
                )
                self.assertEqual(efficiency, r['efficiency'][node])

    def test_efficiency_old_new(self):
        name = 'testdata'
        records = read_resultsfile(name)
        for r in records:
            attack = r['id'].split('(')[0]
            settings = r['settings']
            a = float(settings['alpha'])
            b = float(settings['beta'])
            for node, impacts in r['impacts'].items():
                if not impacts:
                    #  a run may not have detected anything
                    #  and thus have no impacts
                    continue
                a = sum(settings['weights'][attack].values())
                if a != 0:
                    w = {k: w * (1 / a) for k, w in
                         settings['weights'][attack].items()}
                efficiency = subj.calc_efficiency(
                    attack,
                    impacts,
                    w,
                    r['thresholds'][node],
                    int(settings['timeout']),
                    int(r['total_cost']),
                    int(settings['budget']),
                    recovered=not r['timeout'],
                    alpha=a * (1 - b),  # 0 < a < 1 - beta
                    beta=b  # division point
                )

                #change alpha
                efficiency2 = subj.calc_efficiency2(
                    attack,
                    impacts,
                    settings['weights'][attack],
                    r['thresholds'][node],
                    settings['negates'],
                    int(settings['timeout']),
                    int(r['total_cost']),
                    int(settings['budget']),
                    recovered=not r['timeout'],
                    alpha=None,
                    beta=b  # division point
                )
                self.assertEqual(efficiency, efficiency2)

if __name__ == '__main__':
    unittest.main()
