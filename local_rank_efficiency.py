import logging                         
import json
import time
import math
import numpy
from time import sleep
from asyncio import CancelledError     
from json import dumps                 
from asyncio import get_event_loop     
from hbmqtt.client import MQTTClient   
from hbmqtt.mqtt.constants import QOS_0
from signal import SIGTERM 

from integrate import integrate, calc_efficiency
                                       
mqtt_cli = MQTTClient()                     

                                            
async def monitor_domain():                      
    """Monitor local domain metadata"""          
    await mqtt_cli.subscribe([('#', QOS_0)])     
                                                 
                                                 
async def mqtt_receive(timeout=None):            
    msg = await mqtt_cli.deliver_message(timeout)
    packet = msg.publish_packet           
    k = packet.variable_header.topic_name 
    v = packet.payload.data.decode('utf8')
    return k, v                        
                                       
                                             
async def gather_incoming(c, metadata):       
    await c.subscribe([('#', QOS_0)])         
    while True:                               
        msg = await c.deliver_message()       
        packet = msg.publish_packet           
        k = packet.variable_header.topic_name 
        v = packet.payload.data.decode('utf8')
        try:                          
            await metadata.put((k, v))
        except CancelledError:    
            break                                  
    logging.info('monitor: ended')                 
    await c.disconnect()                           

hist_data = {} 

def rank(attack, collab, defence, efficiency):
    hist_data.setdefault((attack, collab, defence),[]).append(efficiency)
    return float(numpy.mean(hist_data[(attack, collab, defence)]))

                                                   
class EfficiencyAgent:                                                

    def __init__(self):
        self.thresh = {}
        self.attack_class = None
        self.running = True                              
        self.recovered = False
        self.detected = False
        self.done = False
        self.success = False
        self.start = 0
        self.timeout = 60 
        self.defense = None
        self.collab = -1
        self.levels = { 'local': 0, 'upstream' : 1,  'alliance' : 2}
        self.watchlist = ['logins', 'logfails', 'sales']
        self.negate_list = ['sales']
        self.defense = { 'nfv', 'filter', 'rate' }
        self.classifications = {'congestion': {'defense': ['filter', 'rate'],
                                              'metrics': ['sales'],
                                              'weights': ['1.0']},
                               'password_bruteforce': {'defense': ['nfv'], 
                                             'metrics': ['logfails', 'logins'],
                                             'weights': [0.2, 0.8]}
                              }
                            

        self.values = {}

    def process_metric(self, metric, v):
        t = time.time()
        try:
            j_v = json.loads(v)
            if isinstance(j_v, dict):
                if len(j_v) == 0:
                    self.values.setdefault(metric,{'values':[], 'times':
                                              []})['values'].append(0.0)
                else:
                    self.values.setdefault(metric,{'values':[], 'times': []})['values'].append(
                        sum(j_v.values()))
            else:
                self.values.setdefault(metric,{'values':[], 'times': []})['values'].append(j_v)
        except ValueError:
            print('unable to parse:', v)
        self.values[metric]['times'].append(t)

    async def sarnet_loop(self):                           
            await mqtt_cli.connect('mqtt://127.0.0.1/')
            #vt = ValueTracker(mqtt_cli, 'sarnet/')
                                                
            await monitor_domain()              
                                                
            while self.running:                      
                try:                            
                    k, v = await mqtt_receive(3)
                except TimeoutError:           
                    continue                   
                service_name='service64'
                if k.startswith('c/' + service_name):
                    if self.detected and not self.recovered: 
                        for metric in self.watchlist:
                            if k.startswith('c/' + service_name + '/' +  metric):
                                self.process_metric(metric, v)
                elif k.startswith('cmd/sarnet/collaboration'):
                    self.collab = self.levels[v]
                elif k.startswith('sarnet/log'):
                    pass
                    #poor way to retrieve defence
                    v_p = json.loads(v)
                    if 'deploy' in v_p.keys():
                        self.defense = v_p['deploy']
                elif k.startswith('sarnet/thresh/') and k.endswith(service_name):
                    metric = k[len('sarnet/thresh/'):-len('@'+ service_name)]
                    self.thresh[metric]= float(v)
                    print('threshold', metric, '->', self.thresh[metric])
                elif k.startswith('sarnet/a/'):
                    if v == 'unhealthy':
                        self.start = time.time()
                        self.values = {} 
                        self.attack_class = k[len('sarnet/a/'):]
                        #self.defense = self.classifications[self.attack_class]['defense']
                        #self.watchlist = self.classifications[self.attack_class]['metrics']
                        print('attack detected', self.attack_class)
                        self.detected = True
                    elif self.detected and v == 'healthy':
                        print('recovered')
                        self.recovered = True
                    elif not v: 
                        print('reset')
                        self.recovered = False
                        self.detected = False
                                            
                if self.recovered: 
                    self.recovered = True
                    self.detected = False
                    self.success = True 
                    self.done = True

                
                if self.detected and (time.time() > self.start + self.timeout):
                    print('timed out', self.timeout)
                    self.success = False
                    self.done = True

                if self.done: 
                    print('recovered=', self.recovered, 'collab level=',
                          self.collab, 'defence=', self.defense)
                    impacts = {}
                    for t in self.classifications[self.attack_class]['metrics']:
                        negate = False
                        start = self.start
                        #start = self.values[t]['times'][0]
                        times_rel = [ti - start for ti in self.values[t]['times']]
                        if t in self.negate_list:
                            negate=True
                        impacts[t] = integrate(self.thresh[t],
                                        times_rel,
                                        self.values[t]['values'], 
                                        times_rel[0], 
                                        times_rel[-1],
                                        normalize=False, negate=negate)

                       # print(t, len(self.values[t]['values']), self.values[t]['values'])
                    weights={k: v for k, v in
                             zip(self.classifications[self.attack_class]['metrics'],
                                 self.classifications[self.attack_class]['weights'])}
                       # print(t, len(self.values[t]['times']), times_rel)
                    thresh = {k: v for k, v in self.thresh.items()}
                    efficiency = calc_efficiency(self.attack_class, impacts,  weights,
                                          thresh, self.timeout)
                    glob_eff = rank(self.attack_class, self.defense, self.collab, efficiency)

                    for k, v in impacts.items():
                        print('impact({})={}'.format(k,v))
                    print('efficiency({})={}'.format(self.attack_class, efficiency))
                    self.recovered = False
                    self.detected = False
                    self.done = False

                sleep(0.1)
                                        
if __name__ == '__main__':                          
    def sarnet_sigterm():
        logging.info('Received SIGTERM')
        agent.running = False
    agent = EfficiencyAgent()
    loop = get_event_loop()
    loop.add_signal_handler(SIGTERM, sarnet_sigterm)
    loop.remove_signal_handler(SIGTERM)
    loop.run_until_complete(agent.sarnet_loop())
    loop.run_until_complete(mqtt_cli.disconnect())
