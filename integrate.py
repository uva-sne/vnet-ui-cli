import logging
import numpy as np
from scipy.interpolate import interp1d
from scipy.integrate import trapz


def plot(filen, xs, ys):
    with open(filen, 'w') as f:
        for x, y in zip(xs, ys):
            f.write('{} {}\n'.format(x, y))


def integrate1(threshold, xs, ys, start, end, negate=False,
               normalize=False, debug=False):
    if end > start:
        # get the datapoints between Detect and Recover_init
        try:
            xs, ys = zip(*[(x, y) for x, y in zip(xs, ys)
                           if x >= start and x <= end])
        except ValueError:
            #raise ValueError('no values between start and end')
            print('warn: no values between start and end')
            return 0
        # set an artificial maximum of 2 times the threshold
        ceiling = threshold * 2

        ys = [y if y < ceiling else ceiling for y in ys]
        # set the threshold as 0
        ys = [y - threshold for y in ys]
        # get the integral
        if negate:
            #  sales are negative so we invert among axis
            ys = [-1 * y for y in ys]
        #  we dont care about anything negative
        ys = [y if y > 0 else 0 for y in ys]
        ig = trapz(ys, xs)

        if ig == 0 and debug:
            print('integrate xs,', xs)
            print('integrate ys', ys)
        if normalize:
            return ig / (threshold * (end - start))
            return ig / (threshold * (end - start))
            #return ig/threshold
        else:
            return ig


def integrate2(threshold, xs, ys, start, end, negate=False,
               normalize=False, debug=False, discard_negative=True):
    if len(ys) == 1:
        return 0

    if end <= start:
        return 0

    if len(xs) != len(ys):
        logging.warn('integrate len(xs) and len(ys) are unequal')

    # get the datapoints between start and end
    xs, ys = zip(*[(x, y) for x, y in zip(xs, ys) if x >= start and x <= end])

    # set an artificial maximum of 2 times the threshold
    ceiling = threshold * 2

    ys = [y if y < ceiling else ceiling for y in ys]
    # set the threshold as 0
    ys = [threshold - y for y in ys]
    # get the integral
    if negate:
        ys = [-1 * y for y in ys]  # sales are negative so we invert among axis

    if discard_negative:
        ys = [y if y > 0 else 0 for y in ys]

    ig = trapz(ys, xs)

    if normalize:
        return ig / (threshold * (end - start))
    return ig


def integrate(*args, **kwargs):
    #return integrate1(args, kwargs)
    # This mimics behaviour of integrate1
    if 'negate' in kwargs:
        kwargs['negate'] = not kwargs['negate']
    else:
        args = list(args)
        args[5] = not args[5]  # flip the 'negate' bool
    return integrate2(*args, **kwargs)


def calc_avg_measurements(values, datapoints=40):
    #xs, ys = values

    #print (ys)

    xnew = np.linspace(0, datapoints, num=datapoints, endpoint=True)
    #print([(xs, ys) for xs, ys, in values])
    normalized = [interp1d(xs, ys, bounds_error=False)(xnew)
                  for xs, ys, in values]
    result = [sum(v) / len(values) for v in zip(*list(normalized))]
    return result


def calc_efficiency_simple(impact, thresh, timeout):
    efficiency = 1 - impact / (thresh * timeout)
    return efficiency


def calc_metric_efficiency(impact, thresh, negative, timeout):
    """Compute efficiency of single metric"""
    max_efficiency = float(thresh * timeout)  # we want floats
    if impact > max_efficiency:
        print('calc_metric_efficiency', impact, thresh, negative, timeout)
        raise ValueError('impact is more than max_efficiency')
    if negative:
        efficiency = float(max_efficiency - impact) / max_efficiency
    else:
        efficiency = impact / max_efficiency
    return efficiency


def calc_metric_efficiencies(impacts, threshs, weights, negatives, timeout):
    """Compute efficiency of multiple metrics in dict"""
    efficiencies = {}
    assert len(weights) < len(impacts), 'not enough impacts'
    assert len(weights) < len(threshs), 'not enough threshs'
    assert len(weights) <= len(negatives), 'not enough negatives'
    #  lenght checking does not cover for mismatching keys
    #  can check whether keys are subset of, in any case it will raise
    #  exception.
    for k in weights.keys():
        efficiencies[k] = calc_metric_efficiency(impacts[k], threshs[k],
                                                 negatives[k], timeout)
    return efficiencies


def calc_cm_efficiency(metric_efficiencies, weights):
    """Compute efficiency of countermeasure given efficiencies"""
    sum_weights = float(sum(weights.values()))
    if sum_weights > 1.0:
        raise ValueError('sum weights > 1')
    elif sum_weights < 1.0:
        raise ValueError('sum weights < 1')
    elif sum_weights < 0:
        raise ValueError('sum weights < 0')

    efficiency = 0.0
    for k in weights.keys():
        efficiency += float(weights[k]) * float(metric_efficiencies[k])
    return efficiency


def calc_def_efficiency(cm_efficiency, costs, budget, recovered=True,
                        alpha=0.5, beta=0.5):
    assert alpha <= 1 - beta, 'Alpha larger than 1-beta'

    if cm_efficiency < 0 or cm_efficiency > 1:
        raise ValueError('cm_efficiency out of bounds [0,1]')

    efficiency = 0
    a = alpha
    b = beta
    e = cm_efficiency
    c = min([(budget - costs) / budget, 1])  # ensure costs are bound

    if recovered:
        efficiency = b + a * e + (1 - b - a) * c
    else:
        efficiency = a * (b / (1 - b)) * e + (1 - b - a) * (b / (1 - b)) * c

    if not (0 < efficiency < 1):
        logging.warn('efficiency out of bounds %s', efficiency)
    return efficiency


def calc_efficiency(attack, impacts, weights, threshs, timeout, costs,
                    budget, recovered=True, alpha=0.5, beta=0.5):
    negatives = {k: True if k == 'sales' else False for k, v in weights.items()}
    m_eff = calc_metric_efficiencies(impacts, threshs, weights, negatives,
                                     timeout)
    cm_efficiency = calc_cm_efficiency(m_eff, weights)
    full_efficiency = calc_def_efficiency(cm_efficiency, costs, budget,
                                          recovered, alpha, beta)
    return full_efficiency


def calc_efficiency2(attack, impacts, weights, threshs, negates, timeout,
                     costs, budget, recovered=True, alpha=0.5, beta=0.5):
    b = beta
    c = min([(budget - costs) / budget, 1])  # ensure costs are bound
    ax = []
    ay = []

    #  scale the weights to values between 0 and 1 - beta
    weights = {k: v * (1 - b) for k, v in weights.items()}

    if 'costs' in weights.keys():
        if float(sum(weights.values())) != (1 - b):
            raise ValueError("sum alphas + cost != beta")
        weights.pop('costs')
    else:
        if float(sum(weights.values())) > (1 - b):
            raise ValueError("sum alphas larger than beta")

    m_eff = calc_metric_efficiencies(impacts, threshs, weights, negates,
                                     timeout)
    for k, v in m_eff.items():
        if negates[k]:
            ay.append((weights[k], v))
        else:
            ax.append((weights[k], v))

    sum_alphas = sum([a for a, _ in ax] +
                     [a for a, _ in ay])

    assert sum_alphas <= 1 - b, "sum alphas larger than beta"

    efficiency = 0
    b_sep = 1  # separates beta :)

    if not recovered:
        b_sep = (b / (1 - b))
    else:
        efficiency = b

    t1 = sum([a * b_sep * y for a, y, in ay])
    t2 = sum([a * b_sep * x for a, x, in ax])
    t3 = (1 - b - sum_alphas) * b_sep * c

    efficiency = efficiency + t1 + t2 + t3

    assert efficiency >= 0 and efficiency <= 1, "efficiency out of bounds"
    return efficiency
